.PHONY: date generate

DATETAG := $(shell date +'%Y-%m-%d_%H%M%S')

generate: date
	docker build . -t alpacabalena/rust_onvif_dev_env:${DATETAG}
	docker push alpacabalena/rust_onvif_dev_env:${DATETAG}
date:
	@echo ${DATETAG}

