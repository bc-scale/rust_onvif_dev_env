# rust_onvif_dev_env



## Name
rust_onvif_dev_env

## Description
Simple Makefile and Dockerfile to generate a new rust_dev_env image. The images are at https://hub.docker.com/repository/docker/alpacabalena/rust_dev_env.

I'm essentially documenting things and tools I have tried to easily access them and reference them in the future.

Feel free to learn from the code here and reuse it. You'll have changes to do to the Makefile if you want to publish your image as you won't be able to publish in alpacabalena.

## Installation
I've used Ubuntu 20.04 on which I installed git and docker. Then, I ran this in a bash:
```
git clone https://gitlab.com/a10332/rust_onvif_dev_env.git
cd rust_onvif_dev_env
# Open the Makefile, read it, understand it and modify it to publish to your own repository on dockerhub.
make
```
Read the Makefile and Dockerfile to know more...

## License
[MIT](LICENSE)

